papers:
  - title: "Integração Contínua com Software Livre: Um relato de implantação na Fundação de Amparo a Pesquisa do Estado de Alagoas – FAPEAL"
    abstract: "A tarefa de integrar artefatos é inerente ao processo de desenvolvimento de software, onde todas as partes que estão sendo trabalhadas pelos diferentes membros da equipe são colocadas juntas e é feita uma verificação se o sistema está funcional, conforme os requisitos. Visto isso, a Integração Contínua surge como uma estratégia de unir os artefatos usados no processo de desenvolvimento de software de maneira constante e em ciclos curtos. Assim, este trabalho tem por objetivo demonstrar o processo de adoção da prática de IC utilizando software livre na Fundação de Amparo a Pesquisa do Estado de Alagoas - FAPEAL, abordando seus conceitos e demonstrando os benefícios de sua utilização."
    file: "paper1.pdf"
    code: 1
    authors:
      - name: Felipe B. de Queiroz
        institution: FAPEAL
      - name: Leonardo F. M. de oliveira
        institution: FAPEAL
  - title: "Processo Demoiselle: um processo livre para desenvolvimento de software para e-Gov"
    abstract: "A qualidade do processo pode influenciar significativamente na qualidade do produto final. Para a maioria das organizações, processos de desenvolvimento de software devem ser tecnologicamente competitivos e adaptáveis e devem ajudar a gerar produtos que atendam aos requisitos dos usuários e do negócio. Esse cenário não é diferente se o assunto é o desenvolvimento de soluções para o governo. Neste artigo apresentamos o processo de desenvolvimento Demoiselle, que integra boas práticas de métodos ágeis e possui foco em arquitetura e em antecipação de testes, podendo ser utilizado como padrão para desenvolver sistemas para o governo."
    file: "paper2.pdf"
    code: 2
    authors:
      - name: Viviane Malheiros
        institution: SERPRO
      - name: Ronaldo Agra
        institution: SERPRO
      - name: Alisson Andrade
        institution: SERPRO
  - title: "Software Livre como Objetos de Aprendizagem Generalizáveis do Projeto CONDIGITAL"
    abstract: "O objeto central deste projeto de pesquisa e desenvolvimento é de utilizar o estado-da-arte em termos de Tecnologia Educacional baseada em Software Livre e suas áreas associadas para desenvolver novos Objetos de Aprendizagem e dar manutenção para as ferramentas genéricas responsáveis pelo apoio computacional ao projeto nacional CONDIGITAL para escolas públicas brasileiras que atuam no nível fundamental médio. A iniciativa é conduzida sob a forma de convênios entre o MEC e membros de parceria."
    file: "paper3.pdf"
    code: 3
    authors:
      - name: Alexandre Direne
        institution: UFPR
      - name: Diego Marczal
        institution: UFPR
      - name: Jonatas Teixeira
        institution: UFPR
      - name: Felipe Moreschi
        institution: UFPR
      - name: Danilo Picolotto
        institution: UFPR
      - name: Derik Silva
        institution: UFPR
      - name: Luan Santos
        institution: UFPR
      - name: Raphael Andrade
        institution: UFPR
      - name: Fernando Coelho
        institution: UFPR
      - name: Jorge Salvi
        institution: UFPR
      - name: Gabriel Ramos
        institution: UFPR
      - name: Andrey Pimentel
        institution: UFPR
  - title: "Uso de software livre para gestão do serviço de atendimento ao usuário de TI no INMETRO"
    abstract: "Este artigo é um estudo de caso sobre a implantação do software livre GLPI (Gestionnaire Libre de Parc Informatique), em conjunto com o processo de implantação do serviço de atendimento ao usuário de TI no INMETRO, nos moldes da Instrução Normativa no 04/2008/MPOG-SLTI. O texto tem como abordagem e justificativa o novo modelo para contratação de serviços de Tecnologia da Informação do governo federal, apresenta o processo e as ferramentas de software livre utilizadas para o caso. Além disso, como benefício, identifica-se possíveis pontos de melhoria no GLPI com suas funcionalidades utilizadas e necessárias para o sucesso da adoção da solução apresentada."
    file: "paper4.pdf"
    code: 4
    authors:
      - name: Eduardo M. Abreu
        institution: Apex-Brasil
      - name: Sandra A. Dias
        institution: INMETRO
      - name: Luiz C. Dalcorno
        institution: INMETRO
      - name: Fabiano D. Lanini
        institution: INMETRO
      - name: Angela B. Albarello
        institution: ESAF
  - title: "Implementing a modern API for CDS/ISIS, a classic semistructured NoSQL database"
    abstract: "CDS/ISIS is a family of semistructured, \"NoSQL\" database products created by Unesco and used at the SciELO digital library as well as thousands of academic libraries since the 1980s. This paper describes how a database-independent API is being developed to allow the LILACS bibliographic methodology created by BIREME to be implemented over CDS/ISIS and modern semistructured databases such as MongoDB and CouchDB."
    file: "paper5.pdf"
    code: 5
    authors:
      - name: Luciano G. S. Ramalho
        institution: USP
  - title: "pgGrid: uma Implementação de Fragmentação de Dados para o PostgreSQL"
    abstract: "À medida que as organizações crescem, também cresce a necessidade de armazenar grandes massas de dados e organizá-los de uma forma que favoreça sua recuperação. A proposta deste trabalho é oferecer uma extensão ao sistema gerenciador de banco de dados (SGBD) PostgreSQL que permita a fragmentação e a distribuição dos dados da forma mais conveniente em vários servidores de banco de dados. Para isso foi necessário modificar a ferramenta \"pgcluster\" a fim de gerenciar a localização dos dados no sistema distribuído e otimizar as consultas. Além disso, foi proposta uma extensão à linguagem de definição de dados (DDL) para a definição dos parâmetros da distribuição dos dados e dos \"sítios\" que formam o sistema de banco de dados distribuído (SBDD)."
    file: "paper6.pdf"
    code: 6
    authors:
      - name: Gustavo A. Tonini
        institution: UFSC
      - name: Frank Siqueira
        institution: UFSC
  - title: "SatBudgets: Projeto Conceitual de Satélites Baseado em Conhecimento e Dirigida a Modelos"
    abstract: "Satélites estão se tornando cada vez mais complexos, fazendo com que decisões técnicas do projeto interdisciplinar impactem custos e prazos. Uma solução proposta é apoiar a fase de projeto conceitual de satélite com adoção de uma engenharia dirigida por modelo (MDE - Model Driven Engineering). Entretanto, softwares de Engenharia de Sistemas Espaciais geralmente podem ser sujeitos a embargo tecnológico ou ter alto custo. Este trabalho visa atenter esta demanda empregando somente software livre e a linguagem SysML (Systems Modeling Language) para implantar MDE através do desenvolvimento de uma ferramenta de software baseado em conhecimento, denominada SatBudgets. A metodologia MDE é geral o bastante para ser aplicada a qualquer projeto de satélite como o ITASAT, um satélite universitário aqui adotado como estudo de caso."
    file: "paper7.pdf"
    code: 7
    authors:
      - name: Bruno B. F. Leonor
        institution: INPE
      - name: Walter A. Santos
        institution: INPE
      - name: Stephan Stephany
        institution: INPE
  - title: "Setfon: Sistema Open Source para produção e organização de Semioetiquetas Fonológicas"
    abstract: "Setfon é um sistema de informação web livre para coleta de dados em pesquisas sobre a fala. O sistema aborda o problema central através de componentes e nasceu da necessidade de aumentar significativamente a quantidade de dados acústico-fonológicos para atender a demandas de estudos estatísticos de expressão da emoção e de estilo. Além disso, por ser on-line, o Setfon permite a criação um banco de dados nacional sobre o tema, compartilhável entre pesquisadores das ciências da fala."
    file: "paper8.pdf"
    code: 8
    authors:
      - name: Ana Cristina Fricke Matte
        institution: UFMG
      - name: Rubens Takiguti Ribeiro
        institution: TecnoLivre/UFLA
  - title: "Avaliando a Qualidade de Conjuntos de Teste de Software de Código Aberto por meio de Critérios de Teste Estruturais"
    abstract: "QualiPSo (Quality Platform for Open Source Software) is an international project investigating Free/Libre/Open Source Software (FLOSS) products to define quality requirements that are important in establishing the trustworthiness of these products. One of the supported activities of QualiPSo aims at evaluating the quality of the test sets developed by the FLOSS community. This paper presents the results of employing structural testing criteria as a measure of quality for such a test sets aiming at identifying the state-of-practice played by the FLOSS community and contributing to the establishment of an incremental testing strategy to evolve the test sets."
    file: "paper9.pdf"
    code: 9
    authors:
      - name: Adriana Rocha
        institution: UFG
      - name: André Mesquita Rincon
        institution: UFG/UNITINS/IFTO
      - name: Márcio Eduardo Delamaro
        institution: USP
      - name: José Carlos Maldonado
        institution: USP
      - name: Auri Marcelo Rizzo Vincenzi
        institution: UFG
  - title: "Extensões para Refatoração de Código Fortran no Eclipse"
    abstract: "Refatoração é uma técnica de engenharia de software que visa aplicar mudanças internas no código-fonte de aplicações, sem afetar seu comportamento observável. Na computação científica, onde existem muitos códigos legados, a refatoração é pouco explorada, pois tais códigos são escritos em linguagens não orientadas a objetos, como Fortran. Este trabalho explora tal lacuna através do desenvolvimento e disponibilização de extensões de refatoração para o ambiente integrado de desenvolvimento Eclipse, utilizando-se do framework Photran (um plugin para programação Fortran integrado ao Eclipse)."
    file: "paper10.pdf"
    code: 10
    authors:
      - name: Bruno B. Boniati
        institution: UFSM
      - name: Gustavo Rissetti
        institution: UFSM
      - name: Andrea S. Charão
        institution: UFSM
      - name: Eduardo K. Piveta
        institution: UFSM
  - title: "Nodipo: ferramenta de levantamento colaborativo de requisitos para software livre"
    abstract: "Este artigo apresenta a ferramenta de levantamento colaborativo de requisitos Nodipo, com a qual se objetiva suprir áreas de aplicação na sociedade em que o software livre é ausente ou pouco presente. Baseada na Web 2.0 e em estudos que levam em conta a participação de usuários finais no processo de elicitação de requisitos e na modelagem de software, a estrutura da ferramenta possui elementos que viabilizam a interação e a colaboração entre os próprios usuários finais e os demais participantes, de forma que os mesmos possam compartilhar conhecimento sobre o domínio da aplicação e elicitar requisitos com um nível mínimo de formalização."
    file: "paper11.pdf"
    code: 11
    authors:
      - name: José Eduardo de Lucca
        institution: UFSC
      - name: Yuri Gomes Cardenas
        institution: UFSC
  - title: "Spider-MPlan: Uma Ferramenta para Apoio ao Processo de Medição do MPS.BR"
    abstract: "Spider-MPlan é uma ferramenta de software livre para apoio ao processo de Medição constante nível F do programa MPS.BR. Este artigo apresenta o fluxo de negócio sistematizado da ferramenta, bem como o seu propósito, contexto de funcionamento e os resultados que podem ser obtidos com a utilização deste produto."
    file: "paper12.pdf"
    code: 12
    authors:
      - name: Bernardo José da Silva Estácio
        institution: UFPA
      - name: Sandro Ronaldo Bezerra Oliveira
        institution: UFPA
  - title: "certificaPET: Sistema Gerenciador de  Certificados de Eventos em Formato Digital"
    abstract: "A utilização de certificados de participação em eventos em formato digital substitui o método tradicional, em meio impresso, com vantagens, tornando necessário o uso de ferramentas computacionais para auxiliar o controle desses certificados. Este artigo trata do desenvolvimento do certificaPET, um aplicativo Web para a geração, armazenamento, disponibilização e validação de certificados de participação em eventos, em formato digital. O sistema foi desenvolvido usando ferramentas abertas, e é distribuído como Software Livre."
    file: "paper13.pdf"
    code: 13
    authors:
      - name: Adriano Pereira
        institution: UFSM
      - name: Vinícius Garcia Pinto
        institution: UFSM
      - name: Andrea Schwertner Charão
        institution: UFSM
  - title: "Software livre baseado na web para estimativa numérica de dados meteorológicos"
    abstract: "Este estudo visa desenvolver um sistema web livre para estimativa numérica de dados meteorológicos no estado do Rio Grande do Sul, com base nas estações climáticas automáticas de superfície do Instituto Nacional de Meteorologia. Para o desenvolvimento da aplicação, foram utilizadas as linguagens de programação PHP e JavaScript, além de um banco de dados MySQL. O método para interpolação da ponderação do inverso da distância (com a distância elevada ao expoente cinco) foi usado como um modelo estimador de dados, uma vez que apresentou o melhor desempenho para as seguintes variáveis: temperatura, umidade relativa, pressão atmosférica e ponto de orvalho. Foi desenvolvida uma base de dados SWIM e o sistema web, os quais podem ser usados em diversas atividades humanas, principalmente na agricultura como suporte para a tomada de decisões."
    file: "paper14.pdf"
    code: 14
    authors:
      - name: Rafael C. Ferraz
        institution: UFSM
      - name: Angélica R. C. de Souza
        institution: UFSM
      - name: Adroaldo D. Robaina
        institution: UFSM
      - name: Marcia X. Peiter
        institution: UFSM
  - title: "um Portal Web Livre para Disseminação de Informações sobre Sistemas Embarcados Críticos"
    abstract: "A disponibilização de informações e compartilhamento de conhecimentos, inclusive de cunho científico, por meio da Internet têm-se tornado extremamente relevantes para a sociedade atual. Assim, o principal objetivo deste artigo é apresentar a RIPSEC (Rede de Inovação e Prospecção em Sistemas Embarcados Críticos), uma rede de colaboração disponibilizada por meio de um portal web, cujo código fonte é livre, e que visa agregar informações sobre pesquisas, desenvolvimento e uso de Sistemas Embarcados Críticos (SEC). Essa rede é uma das contribuições do INCT-SEC, um projeto de pesquisa que visa principalmente elevar o nível de conhecimento, competência e qualidade no Brasil sobre o desenvolvimento de SEC."
    file: "paper15.pdf"
    code: 15
    authors:
      - name: Ricardo A. M. Reimão
        institution: USP
      - name: Elisa Y. Nakagawa
        institution: USP
      - name: Thiago Bianchi
        institution: USP
      - name: José C. Maldonado
        institution: USP
  - title: "XadrezLivre: Proposta de Arquitetura de Ambiente de Jogos baseado em XMPP com clienteWeb"
    abstract: "O xadrez além de servir como entretenimento, apresenta pontos positivos no desenvolvimento do raciocínio lógico, no aprendizado e na tomada de decisões. Dentro desse contexto de aprendizado foi desenvolvido o projeto Apoio Computacional ao Ensino de Xadrez nas Escolas e como resultado do desenvolvimento do projeto surgiu o ambiente de jogo XadrezLivre. Atualmente o ambiente suporta mais de 80.000 usuários e aproximadamente 1.500 partidas por dia. Esse trabalho apresenta a arquitetura e a implementação do ambiente assim como as ferramentas em Softwares Livre utilizadas e desenvolvidas."
    file: "paper16.pdf"
    code: 16
    authors:
      - name: Rubens Massayuki Suguimoto
        institution: UFPR
      - name: Luis Carlos Erpen de Bona
        institution: UFPR
      - name: Alexandre Direne
        institution: UFPR
  - title: "Análise Experimental Comparativa de Algoritmos de Alocação de Memória de Código Aberto"
    abstract: "Operações de alocação de memória estão entre as mais ubíquas em programas de computador. O desempenho dos alocadores que implementam essas operações é de suma importância para o desempenho global da aplicação, embora muitas vezes seja negligenciado. Esse trabalho apresenta um estudo comparativo entre cinco alocadores de memória de propósito geral e de código aberto. Diferente de outros trabalhos na área, baseados em testes de benchmark com difícil generalização para aplicações reais, esse trabalho avalia o desempenho dos alocadores no software MySQL. Os resultados mostram que o MySQL apresentou melhor desempenho em conjunto com o jemalloc, especialmente quando comparado com o alocador padrão da glibc."
    file: "paper17.pdf"
    code: 17
    authors:
      - name: Rivalino Matias Jr.
        institution: UFU
      - name: Autran Macêdo
        institution: UFU
      - name: Taís Borges Ferreira
        institution: UFU
  - title: "Avaliação Experimental do Índice de Carga Usado no Kernel Linux"
    abstract: "Neste artigo avaliamos experimentalmente a qualidade do loadavg implementado no kernel Linux. A hipótese de pesquisa é de que esse índice de carga não é adequado para caracterizar cenários com significante atividade em nível de kernel. Para alguns tipos de sistemas baseados em Linux (ex. firewals), a carga em nível de kernel pode ser significantemente maior do que em espaço de usuário. Os resultados experimentais mostraram que o loadavg não é capaz de representar apropriadamente cenários com carga elevada em nível de kernel, confirmando a hipótese de pesquisa avaliada."
    file: "paper18.pdf"
    code: 18
    authors:
      - name: Rivalino Matias Jr.
        institution: UFU
      - name: Leonardo Alt
        institution: UFU
      - name: Otávio Augusto
        institution: UFU
  - title: "MDM: Um Software Livre para Configuração de Ambientes Multiterminais"
    abstract: "Um dos principais problemas na configuração automática de ambientes multiterminais é o processo de detecção e associação de hardware. Este processo deve ser o mais automático possível, visando minimizar os custos de instalação e manutenção. Os dispositivos presentes em uma máquina devem ser detectados e associados a pontos de trabalho de forma que cada um possua ao menos um mouse, teclado e monitor. Este artigo descreve tais problemas e suas principais soluções, além de apresentar o Multiseat Display Manager (MDM), uma ferramenta de código aberto que realiza a coniguração automática de multiterminais."
    file: "paper19.pdf"
    code: 19
    authors:
      - name: Aramis Stach Haiduski Fernandes
        institution: UFPR
      - name: Francisco Panis Kaseker
        institution: UFPR
      - name: Lucas Nascimento Ferreira
        institution: UFPR
      - name: Paulo Ricardo Zanoni
        institution: UFPR
      - name: Pedro Eugenio Rocha
        institution: UFPR
      - name: Eduardo Todt
        institution: UFPR
