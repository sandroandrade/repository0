## Program Committee

* Antônio Marinho Barcellos, Universidade do Vale do Rio dos Sinos, Brazil
* Carlos Alberto Heuser, Universidade Federal do Rio Grande do Sul, Brazil
* Denise Bandeira da Silva, Universidade do Vale do Rio dos Sinos, Brazil
* Fernando Santos Osório, Universidade do Vale do Rio dos Sinos, Brazil
* João Batista de Oliveira, Pontifícia Universidade Católica do Rio Grande do Sul, Brazil
* Luciana Porcher Nedel, Universidade Federal do Rio Grande do Sul, Brazil
* Luis Fernando Fortes Garcia, Universidade Feevale, Brazil
* Roland Teodorowitsch, Universidade Luterana do Brasil, Brazil
* Marcelo Pasin, Universidade Federal de Santa Maria, Brazil
* Taisy Weber, Universidade Federal do Rio Grande do Sul, Brazil

## Organizing Committee

General Chair

* Luciana Porcher Nedel (Universidade Federal do Rio Grande do Sul, Brazil)

Program Committee Chair

* Antônio Marinho Barcellos (Universidade do Vale do Rio dos Sinos, Brazil)

Organizing Committee

* Denise Bandeira da Silva (Universidade do Vale do Rio dos Sinos, Brazil)
* Douglas Kellermann (Universidade Feevale, Brazil)
* Roland Teodorowitsch (Universidade Luterana do Brasil, Brazil)
