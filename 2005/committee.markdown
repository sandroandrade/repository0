## National Track Program Committee

* Adenauer Yamin, Universidade Federal de Pelotas/Universidade Católica de Pelotas, Brazil
* Aldebaro Barreto da Rocha Klautau Jr., Universidade Federal do Pará, Brazil
* Alessandro Garcia, Lancaster University, United Kingdom
* Alexandre Ribeiro, Universidade de Caxias do Sul, Brazil
* Andrea Charão, Universidade Federal de Santa Maria, Brazil
* Antônio Jorge Abelém, Universidade Federal do Pará, Brazil
* Benhur Stein, Universidade Federal de Santa Maria, Brazil
* Carla Alessandra Lima Reis, Universidade Federal do Pará, Brazil
* Cláudio Resin Geyer, Universidade Federal do Rio Grande do Sul, Brazil
* Cesar De Rose, Pontifícia Universidade Católica do Rio Grande do Sul, Brazil
* Edson Silva Junior, Universidade Federal do Amazonas, Brazil
* Fernando Osório, Universidade do Vale do Rio dos Sinos, Brazil
* Gerson Cavalheiro, Universidade do Vale do Rio dos Sinos, Brazil
* Iara Augustin, Universidade Federal de Santa Maria, Brazil
* Jorge Barbosa, Universidade do Vale do Rio dos Sinos, Brazil
* Kelvin Lopes Dias, Universidade da Amazônia, Brazil
* Luciana Porcher Nedel, Universidade Federal do Rio Grande do Sul, Brazil
* Luciano Paschoal Gaspary, Universidade do Vale do Rio dos Sinos, Brazil
* Luciano Porto Barreto, Universidade Federal da Bahia, Brazil
* Márcia Pasin, Universidade Federal de Santa Maria, Brazil
* Marcos Castilho, Universidade Federal do Paraná, Brazil
* Marilton Aguiar, Universidade Católica de Pelotas, Brazil
* Nabor C. Mendonça, Universidade de Fortaleza, Brazil
* Nara Bigolin, Universidade Luterana do Brasil, Brazil
* Patrícia Machado, Universidade Federal de Campina Grande, Brazil
* Rafael dos Santos, Universidade de Santa Cruz do Sul, Brazil
* Rejane Frozza, Universidade de Santa Cruz do Sul, Brazil
* Ricardo Corrêa, Universidade Federal do Ceará, Brazil

## Organizing Committee

General Chair

* Celso Maciel da Costa (Pontifícia Universidade Católica do Rio Grande do Sul, Brazil)

Organizing Committee

* Alexandre Ribeiro (Universidade de Caxias do Sul, Brazil)
* Denise Bandeira(Universidade do Vale do Rio dos Sinos, Brazil)
* Filipi Vianna (Pontifícia Universidade Católica do Rio Grande do Sul, Brazil)
* Roland Teodorowitsch (Universidade Luterana do Brasil, Brazil)
* Ronaldo Lages (Associação Software Livre, Brazil)

National Track Program Committee Chair

* Rodrigo Quites Reis (Universidade Federal do Pará, Brazil)
