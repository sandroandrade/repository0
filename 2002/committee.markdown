## Program Committee

* Antônio Marinho Barcellos, Universidade do Vale do Rio dos Sinos, Brazil
* Denise Bandeira de Silva, Universidade do Vale do Rio dos Sinos, Brazil
* Fernando Santos Osório, Universidade do Vale do Rio dos Sinos, Brazil
* João Batista Oliveira, Pontifícia Universidade Católica do Rio Grande do Sul, Brazil
* Luciana Porcher Nedel, Universidade Federal do Rio Grande do Sul/Pontifícia Universidade Católica do Rio Grande do Sul, Brazil
* Luis Fernando Forte Garcia, Universidade do Vale do Rio dos Sinos, Brazil
* Mouriac Hallen Diemer, Unidade Integrada Vale do Taquari de Ensino Superior, Brazil
* Roland Teodorowitsch, Universidade Luterana do Brasil, Brazil

## Organizing Committee

General Chair

* César Augusto Fonticiealha de Rose (Pontifícia Universidade Católica do Rio Grande do Sul, Brazil)

Program Committee Chair

* João Batista Oliveira (Pontifícia Universidade Católica do Rio Grande do Sul, Brazil)

Organizing Committee

* Denise Bandeira de Silva (Universidade do Vale do Rio dos Sinos, Brazil)
* Douglas Kellerman (Universidade Feevale, Brazil)
* Luciana Porcher Nedel (Universidade Federal do Rio Grande do Sul/Pontifícia Universidade Católica do Rio Grande do Sul, Brazil)
* Roland Teodorowitsch (Universidade Luterana do Brasil, Brazil)
