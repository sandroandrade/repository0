## National and International Track Program Committee

* Adenauer Yamin, Universidade Federal de Pelotas/Universidade Católica de Pelotas, Brazil
* Alexandre Ribeiro, Universidade de Caxias do Sul, Brazil
* Andrea Charão, Universidade Federal de Santa Maria, Brazil
* Carla Alessandra Lima Reis, Universidade Federal do Pará, Brazil
* Cesar De Rose, Pontifícia Universidade Católica do Rio Grande do Sul, Brazil
* Cristiano André da Costa, Universidade do Vale do Rio dos Sinos, Brazil
* Fernando Osório, Universidade do Vale do Rio dos Sinos, Brazil
* Gerson Cavalheiro, Universidade do Vale do Rio dos Sinos, Brazil
* Iara Augustin, Universidade Federal de Santa Maria, Brazil
* João Cesar Netto, Universidade Federal do Rio Grande do Sul, Brazil
* Jorge Barbosa, Universidade do Vale do Rio dos Sinos, Brazil
* Leonardo Lemes Fagundes, Universidade do Vale do Rio dos Sinos, Brazil
* Lisandro Zambenedetti Granville, Universidade Federal do Rio Grande do Sul, Brazil
* Luciano Paschoal Gaspary, Universidade do Vale do Rio dos Sinos, Brazil
* Luciano Porto Barreto, Universidade Federal da Bahia, Brazil
* Márcia Pasin, Universidade Federal de Santa Maria, Brazil
* Marcos Castilho, Universidade Federal do Paraná, Brazil
* Marilton Aguiar, Universidade Católica de Pelotas, Brazil
* Marinho Pilla Barcellos, Universidade do Vale do Rio dos Sinos, Brazil
* Mario Domenech Goulart, Universidade Católica de Pelotas, Brazil
* Mauricio Lima Pilla, Universidade Católica de Pelotas, Brazil
* Nara Bigolin, Universidade Luterana do Brasil, Brazil
* Olivier Berger, GET/INT, France
* Rejane Frozza, Universidade de Santa Cruz do Sul, Brazil
* Rodrigo Araújo Real, Esteves & Salvador Ltda., Brazil
* Rodrigo Quites Reis, Universidade Federal do Pará, Brazil

## Free Software in University Track Program Committee

* Altigran Soares da Silva, Universidade Federal do Amazonas, Brazil
* Carlos Alfeu Fonseca, Universidade Federal de Minas Gerais, Brazil
* Celso Maciel da Costa, Pontifícia Universidade Católica do Rio Grande do Sul, Brazil
* Elian Machado, Universidade Federal do Ceará, Brazil
* Heitor Strogulski, Universidade de Caxias do Sul, Brazil
* José Riverson Rios, Universidade Federal do Ceará, Brazil
* Javam Machado, Universidade Federal do Ceará, Brazil
* Jose Ramos Gonçalves, Universidade Federal do Ceará, Brazil
* Jussara Musse, Universidade Federal do Rio Grande do Sul, Brazil
* Leandro Rey, Universidade Federal do Rio Grande do Sul, Brazil
* Regina Araujo, Universidade Federal de São Carlos, Brazil

## Organizing Committee

General Chair

* Adenauer Yamin (Universidade Federal de Pelotas/Universidade Católica de Pelotas, Brazil)

Organizing Committee

* Celso Maciel da Costa (Pontifícia Universidade Católica do Rio Grande do Sul, Brazil)
* Cristiano André da Costa (Universidade do Vale do Rio dos Sinos, Brazil)
* Filipi Vianna (Pontifícia Universidade Católica do Rio Grande do Sul, Brazil)
* Lisandro Zambenedetti Granville (Universidade Federal do Rio Grande do Sul, Brazil)
* Roland Teodorowitsch (Universidade Luterana do Brasil, Brazil)

National Track Program Committee Chair

* Luciano Paschoal Gaspary (Universidade do Vale do Rio dos Sinos, Brazil)

International Track Program Committee Chair

* Olivier Berger (GET/INT, France)

Free Software in University Track Program Committee Chair

* Javam Machado (Universidade Federal do Ceará, Brazil)
