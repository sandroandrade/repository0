## Program Committee

* Alex Fábio Pellin, Universidade de Caxias do Sul, Brazil
* Alexandre Moretto Ribeiro, Universidade de Caxias do Sul, Brazil
* Andrea Charão, Universidade Federal de Santa Maria, Brazil
* Antônio Marinho Pilla Barcelos, Universidade do Vale do Rio dos Sinos, Brazil
* Benhur Stein, Universidade Federal de Santa Maria, Brazil
* Celso Maciel da Costa, Pontifícia Universidade Católica do Rio Grande do Sul, Brazil
* Cesar De Rose, Pontifícia Universidade Católica do Rio Grande do Sul, Brazil
* Cirano Iochpe, Universidade Federal do Rio Grande do Sul, Brazil
* Cláudio Fernando Resin Geyer, Universidade Federal do Rio Grande do Sul, Brazil
* Cristina Moreira Nunes, La Salle, Brazil
* Daniel Nehme Muller, Universidade Luterana do Brasil, Brazil
* Daniel Notari, Universidade de Caxias do Sul, Brazil
* Denise Bandeira da Silva, Universidade do Vale do Rio dos Sinos, Brazil
* Edgar Meneghetti, Universidade de Caxias do Sul, Brazil
* Fernando Santos Osório, Universidade do Vale do Rio dos Sinos, Brazil
* Francisco Assis Moreira do Nascimento, Universidade Luterana do Brasil, Brazil
* Gerson Geraldo Homirch Cavalheiro, Universidade do Vale do Rio dos Sinos, Brazil
* Heitor Strogulski, Universidade de Caxias do Sul, Brazil
* João Cesar Netto, Universidade Federal do Rio Grande do Sul, Brazil
* João Ricardo Bittencourt, Centro Universitário Franciscano, Brazil
* Julio Bertolin, Universidade de Passo Fundo, Brazil
* Leonardo Rodriguez Heredia, Universidade Federal do Rio Grande do Sul, Brazil, Brazil
* Lisandro Zambenedetti Granville, Universidade Federal do Rio Grande do Sul, Brazil
* Luciana Porcher Nedel, Universidade Federal do Rio Grande do Sul, Brazil
* Luciano Paschoal Gaspary, Universidade do Vale do Rio dos Sinos, Brazil
* Luciano Porto Barreto, Universidade Federal da Bahia, Brazil
* Marcelo Pasin, Universidade Federal de Santa Maria, Brazil
* Roland Teodorowitsch, Universidade Luterana do Brasil, Brazil
* Simone André da Costa, Universidade do Vale do Rio dos Sinos, Brazil

## Organizing Committee

General Chair

* Celso Maciel da Costa (Pontifícia Universidade Católica do Rio Grande do Sul, Brazil)

Program Committee Chair

* Alexandre Moretto Ribeiro (Universidade de Caxias do Sul, Brazil)

Organizing Committee

* Alex Fábio Pellin (Universidade de Caxias do Sul, Brazil)
* Alexandre Moretto Ribeiro (Universidade de Caxias do Sul, Brazil)
* Denise Bandeira da Silva (Universidade do Vale do Rio dos Sinos, Brazil)
* Edgar Meneghetti (Universidade de Caxias do Sul, Brazil)
* Filipi Vianna (Pontifícia Universidade Católica do Rio Grande do Sul, Brazil)
* Heitor Strogulski (Universidade de Caxias do Sul, Brazil)
